variable "region" {
  type        = string
  default     = "ap-south-1"
  description = "region where to create vpc"
}

variable "vpc-cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "aws assignment_04 vpc cidr vale"
}

variable "tenancy" {
  type        = string
  default     = "default"
  description = "instance tenancy"
}

variable "vpc-tag" {
  type        = string
  default     = "ninja-vpc-01"
  description = "v"
}

variable "pub-sub-cidr" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  description = "public subnet cidr value"
}

variable "pri_sub_cidr" {
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24"]
  description = "priv subnet cidr value"
}

variable "Private-tag" {
  type        = string
  default     = "ninja-priv-sub-0"
  description = "tags for priv subs"
}

variable "azs" {
  type        = list(string)
  description = "Availability Zones"
  default     = ["ap-south-1a", "ap-south-1b"]
}

variable "igw-tag" {
  type        = string
  default     = "ninja-igw-01"
  description = "tag for internet gateway"
}

variable "pub-rt-tag" {
  type        = string
  default     = "public_RT"
  description = "tags for public route table"
}

variable "priv-rt-tag" {
  type        = string
  default     = "private_RT"
  description = "tags for private route table"
}

variable "nat-tag" {
  type        = string
  default     = "ninja-nat-01"
  description = "tag for nat gateway"
}

variable "pub_ports" {
  type        = list(string)
  default     = ["22", "80", "8080"]
  description = "allowed ports for ninja_public_sg "
}

variable "pub_protocol" {
  type        = string
  default     = "tcp"
  description = "protocol type for ninja_public_sg"
}

variable "pub_cidr_block" {
  type        = string
  default     = "0.0.0.0/0"
  description = "cidr block for ninja_public_sg"
}

variable "pri_ports" {
  type        = list(string)
  default     = ["22", "80"]
  description = "allowed ports for ninja_private_sg "
}

variable "pri_protocol" {
  type        = string
  default     = "tcp"
  description = "protocol type for ninja_private_sg"
}

variable "pri_cidr_block" {
  type        = string
  default     = "10.0.1.0/24"
  description = "cidr block for ninja_private_sg"
}

variable "ami" {
  type        = string
  default     = "ami-013cfb4ff88d86446"
  description = "ami for terraform assignment"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "what type of instance you want create"
}

variable "nacl_ports" {
  type        = list(string)
  default     = ["80","22"]
  description = "allow port for privat_subnet_nacl"
}
