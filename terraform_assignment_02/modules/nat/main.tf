
resource "aws_eip" "elastic-ip" {
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip.id
  subnet_id = var.nat_subnet_id
  tags = {
    Name = var.nat-tag
  }
}