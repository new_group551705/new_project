resource "aws_network_acl" "public_nacl" {
  count          = length(var.pub-subnet-id)
  vpc_id = var.vpc_id
  subnet_ids = [var.pub-subnet-id[count.index]]
  
  dynamic "egress" {
    for_each = var.nacl_ports
    iterator = port
   
    content {  
     
    protocol   = "tcp"
    rule_no    = "${port.value + 10}"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = port.value
    to_port    = port.value

  }
  }
  
 dynamic "ingress" {
    for_each = var.nacl_ports
    iterator = port
    content {

    protocol   = "tcp"
    rule_no    = "${port.value + 10}"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = port.value
    to_port    = port.value

  # tags = {
  #   Name = "puclic_nacl"
  # }
}
}
}

resource "aws_network_acl" "private_nacl" {
  count          = length(var.pri_subnet_id)
  vpc_id = var.vpc_id
  subnet_ids = [var.pri_subnet_id[count.index]]

  dynamic "egress" {
    for_each = var.nacl_ports
    iterator = port
    content {
      
    protocol   = "tcp"
    rule_no    = "${port.value + 10}"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = port.value
    to_port    = port.value
 
  }
  }
  dynamic "ingress" {
    for_each = var.nacl_ports
    iterator = port
    content {
    protocol   = "tcp"
    rule_no    = "${port.value + 10}"
    action     = "allow"
    cidr_block = "10.0.1.0/24"
    from_port  = port.value
    to_port    = port.value

  # tags = {
  #   Name = "private_nacl"
  # }
}
}
}

