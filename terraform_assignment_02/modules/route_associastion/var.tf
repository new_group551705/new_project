variable pub_RT_id {
  type        = string

}

variable pri_RT_id {
  type        = string

}

variable pub-subnet-id {
  type        = list(string)
}

variable pri_subnet_id {
  type        = list(string)
}

variable "pub-sub-cidr" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  description = "public subnet cidr value"
}

variable "pri_sub_cidr" {
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24"]
  description = "priv subnet cidr value"
}