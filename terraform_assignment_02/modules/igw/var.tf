variable "igw-tag" {
  type        = string
  default     = "ninja-igw"
}

variable "vpc_id" {
  type        = string
}
