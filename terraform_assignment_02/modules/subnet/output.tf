
output "pub-subnet-id" {
  value = aws_subnet.pub-subnets.*.id
}

output "pri_subnet_id" {
  value = aws_subnet.pri-subnets.*.id
}

output "nat_subnet_id" {
  value       = aws_subnet.pub-subnets[0].id
}


