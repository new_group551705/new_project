
provider "aws" {
  region = var.region
}

########################################################################

resource "aws_vpc" "vpc-01" {
  cidr_block       = var.vpc-cidr
  instance_tenancy = var.tenancy
  tags = {
    Name = var.vpc-tag
  }
}

########################################################################

resource "aws_subnet" "pub-subnets" {
  count             = length(var.pub-sub-cidr)
  vpc_id            = aws_vpc.vpc-01.id
  cidr_block        = var.pub-sub-cidr[count.index]
  availability_zone = var.azs[count.index]

  tags = {
    Name = "ninja-pub-sub-00 ${count.index + 1}"
  }
}

########################################################################

resource "aws_subnet" "pri-subnets" {
  count             = length(var.pri_sub_cidr)
  vpc_id            = aws_vpc.vpc-01.id
  cidr_block        = var.pri_sub_cidr[count.index]
  availability_zone = var.azs[count.index]
  tags = {
    Name = "ninja-priv-sub-00 ${count.index + 01}"
  }
}

########################################################################

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.vpc-01.id
  tags = {
    Name = var.igw-tag
  }
}

resource "aws_eip" "elastic-ip" {
}

########################################################################

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip.id
  subnet_id     = aws_subnet.pub-subnets[0].id
  tags = {
    Name = var.nat-tag
  }
}

########################################################################

resource "aws_route_table" "pub-RT" {
  vpc_id = aws_vpc.vpc-01.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IGW.id
  }
  tags = {
    Name = var.pub-rt-tag
  }
}

resource "aws_route_table_association" "public_subnet_association" {
  count          = length(var.pub-sub-cidr)
  subnet_id      = aws_subnet.pub-subnets[count.index].id
  route_table_id = aws_route_table.pub-RT.id
}

########################################################################

resource "aws_route_table" "pri-RT" {
  vpc_id = aws_vpc.vpc-01.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gw.id
  }
  tags = {
    Name = "Private Route Table"
  }
}

resource "aws_route_table_association" "private_subnet_association" {
  count          = length(var.pri_sub_cidr)
  subnet_id      = aws_subnet.pri-subnets[count.index].id
  route_table_id = aws_route_table.pri-RT.id
}

########################################################################

resource "aws_network_acl" "public_nacl" {
  count      = length(var.pub-sub-cidr)
  vpc_id     = aws_vpc.vpc-01.id
  subnet_ids = [aws_subnet.pub-subnets[count.index].id]

  dynamic "egress" {
    for_each = var.nacl_ports
    iterator = port

    content {

      protocol   = "tcp"
      rule_no    = port.value + 10
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = port.value
      to_port    = port.value

    }
  }

  dynamic "ingress" {
    for_each = var.nacl_ports
    iterator = port
    content {

      protocol   = "tcp"
      rule_no    = port.value + 10
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = port.value
      to_port    = port.value
    }
  }
      tags = {
        Name = "puclic_nacl-${count.index + 1}"
      }
  }

########################################################################

resource "aws_network_acl" "private_nacl" {
  count      = length(var.pri_sub_cidr)
  vpc_id     = aws_vpc.vpc-01.id
  subnet_ids = [aws_subnet.pri-subnets[count.index].id]

  dynamic "egress" {
    for_each = var.nacl_ports
    iterator = port
    content {

      protocol   = "tcp"
      rule_no    = port.value + 10
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = port.value
      to_port    = port.value

    }
  }
  dynamic "ingress" {
    for_each = var.nacl_ports
    iterator = port
    content {
      protocol   = "tcp"
      rule_no    = port.value + 10
      action     = "allow"
      cidr_block = "10.0.1.0/24"
      from_port  = port.value
      to_port    = port.value
    }
  }
      tags = {
        Name = "private_nacl-${count.index + 1}"
      }
}

########################################################################

resource "aws_security_group" "public_sg" {
  name        = "ninja_pub_sg"
  description = "aws terraform assignment public sg"
  vpc_id      = aws_vpc.vpc-01.id
  #availability_zone = var.azs[0]

  dynamic "ingress" {
    for_each = var.pub_ports
    iterator = port
    content {
      description = "rules for public_sg"
      from_port   = port.value
      to_port     = port.value
      protocol    = var.pub_protocol
      cidr_blocks = [var.pub_cidr_block]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

########################################################################

resource "aws_security_group" "private_sg" {
  name        = "ninja_pri_sg"
  description = "aws terraform assignment private sg "
  vpc_id      = aws_vpc.vpc-01.id

  dynamic "ingress" {
    for_each = var.pri_ports
    iterator = port
    content {
      description = "rules for private_sg"
      from_port   = port.value
      to_port     = port.value
      protocol    = var.pri_protocol
      cidr_blocks = [var.pri_cidr_block]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

######################################################

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "pem_key" {
  key_name   = "pem_key"
  public_key = tls_private_key.rsa.public_key_openssh
}

resource "local_file" "ssh_key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "pem_key"
}

#####################################################

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

##########################################################

resource "aws_instance" "bastion_host_server" {
  count                       = length(var.pub-sub-cidr)
  subnet_id                   = aws_subnet.pub-subnets[count.index].id
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  associate_public_ip_address = true
  key_name                    = aws_key_pair.pem_key.key_name
  security_groups             = [aws_security_group.public_sg.id]

  tags = {
    Name = "public-${count.index + 1}"
  }
}

#########################################################


resource "aws_instance" "private_server" {
  count                       = length(var.pri_sub_cidr)
  subnet_id                   = aws_subnet.pri-subnets[count.index].id
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  associate_public_ip_address = false
  key_name                    = aws_key_pair.pem_key.key_name
  security_groups             = [aws_security_group.private_sg.id]

  tags = {
    Name = "private-${count.index + 1}"
  }
}

##########################################################

resource "aws_lb_target_group" "target_group" {
  name        = "TGnginx"
  port        = 80
  target_type = "instance"
  protocol    = "HTTP"
  vpc_id      = aws_vpc.vpc-01.id
}


resource "aws_lb" "loadbalancer" {
  name                       = "test-ALB"
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.public_sg.id]
  subnets                    = aws_subnet.pub-subnets.*.id
  enable_deletion_protection = false
  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group_attachment" "attach_TG" {
  count            = length(var.pri_sub_cidr)
  target_group_arn = aws_lb_target_group.target_group.arn
  target_id        = aws_instance.private_server[count.index].id
  port             = 80
}

resource "aws_lb_listener" "lb_listener_http" {
  load_balancer_arn = aws_lb.loadbalancer.id
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.target_group.id
    type             = "forward"
  }
}

##############################################################

