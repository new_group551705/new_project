
resource "aws_security_group" "public_sg" {
  name        = var.public_sg_name
  description = var.pub_sg_discription
  vpc_id      = var.vpc_id
  
  dynamic "ingress" {
    for_each = var.pub_ports
    iterator = port
    content {
      description = "rules for public_sg"
      from_port   = port.value
      to_port     = port.value
      protocol    = var.pub_protocol
      cidr_blocks = [var.pub_cidr_block]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}

resource "aws_security_group" "private_sg" {
  name        = var.private_sg_name
  description = var.pri_sg_discription
  vpc_id      = var.vpc_id
  
  dynamic "ingress" {
    for_each = var.pri_ports
    iterator = port
    content {
      description = "rules for private_sg"
      from_port   = port.value
      to_port     = port.value
      protocol    = var.pri_protocol
      cidr_blocks = [var.pri_cidr_block]
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

}