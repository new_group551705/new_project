output pub_RT_id {
  value       = aws_route_table.pub-RT.id
}

output pri_RT_id {
  value       = aws_route_table.pri-RT.id
}
