
variable "pub_ports" {
  type        = list(string)
  default     = ["22", "80", "8080"]
  description = "allowed ports for ninja_public_sg "
}

variable "pub_protocol" {
  type        = string
  default     = "tcp"
  description = "protocol type for ninja_public_sg"
}

variable "pub_cidr_block" {
  type        = string
  default     = "0.0.0.0/0"
  description = "cidr block for ninja_public_sg"
}

variable "pri_ports" {
  type        = list(string)
  default     = ["22", "80"]
  description = "allowed ports for ninja_private_sg "
}

variable "pri_protocol" {
  type        = string
  default     = "tcp"
  description = "protocol type for ninja_private_sg"
}

variable "pri_cidr_block" {
  type        = string
  default     = "10.0.1.0/24"
  description = "cidr block for ninja_private_sg"
}

variable "vpc_id" {
  type        = string
}

variable public_sg_name {
  type        = string
  default     = "ninja_pub_sg"
  description = "description"
}

variable pub_sg_discription {
  type        = string
  default     = "aws terraform assignment public sg"
  description = "description for pub_sg"
}

variable private_sg_name {
  type        = string
  default     = "ninja_pri_sg"
  description = "name for private_sg"
}

variable pri_sg_discription {
  type        = string
  default     = "aws terraform assignment private sg"
  description = "description for pri_sg"
}
