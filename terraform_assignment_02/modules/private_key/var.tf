variable algorithm {
  type        = string
  default     = "RSA"
  description = "provide algorithm for key"
}

variable key_name {
  type        = string
  default     = "pem_key"
  description = "provide name for key"
}

