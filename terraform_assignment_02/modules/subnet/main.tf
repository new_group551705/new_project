resource "aws_subnet" "pub-subnets" {
  count  = length(var.pub-sub-cidr)
  vpc_id = var.vpc_id
  cidr_block        = var.pub-sub-cidr[count.index]
  availability_zone = var.azs[count.index]

  tags = {
    Name = "ninja-pub-sub-00 ${count.index + 1}"
  }
}

resource "aws_subnet" "pri-subnets" {
  count  = length(var.pri_sub_cidr)
  vpc_id = var.vpc_id
  cidr_block        = var.pri_sub_cidr[count.index]
  availability_zone = var.azs[count.index]
  tags = {
    Name = "ninja-priv-sub-00 ${count.index + 01}"
  }
}