variable "vpc_id" {
  type        = string
}

# variable nacl_pub_ports {
#   type        = list(string)
#   default     = ["80","22"]
#   description = "allow ports for public_subnet_nacl "
# }
 
variable "nacl_ports" {
  type        = list(string)
  default     = ["80","22"]
  description = "allow port for privat_subnet_nacl"
}
 
variable pub-subnet-id {
  type        = list(string)

}

variable pri_subnet_id {
  type        = list(string)

} 