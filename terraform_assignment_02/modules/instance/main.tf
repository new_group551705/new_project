

resource "aws_instance" "bastion_host_server" {
  count          = length(var.pub-subnet-id)
  subnet_id = var.pub-subnet-id[count.index]
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  security_groups = [var.pub_security_id]
  associate_public_ip_address = true
  
  tags = {
    name = var.public_instance_name
  }

}

resource "aws_instance" "private_server" {
  count          = length(var.pri_subnet_id)
  subnet_id = var.pri_subnet_id[count.index]
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  security_groups = [var.pri_security_id]
  associate_public_ip_address = false

  tags = {
    name = "private_server"
  }

} 