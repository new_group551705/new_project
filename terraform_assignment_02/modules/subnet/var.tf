variable "pub-sub-cidr" {
  type        = list(string)
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  description = "public subnet cidr value"
}

variable "pri_sub_cidr" {
  type        = list(string)
  default     = ["10.0.4.0/24", "10.0.5.0/24"]
  description = "priv subnet cidr value"
}

variable "Private-tag" {
  type        = string
  default     = "ninja-priv-sub-0"
  description = "tags for priv subs"
}

variable "igw-tag" {
  type        = string
  default     = "ninja-igw-01"
  description = "tag for internet gateway"
}

variable "azs" {
  type        = list(string)
  description = "Availability Zones"
  default     = ["ap-south-1a", "ap-south-1b"]
}




variable "nat-tag" {
  type        = string
  default     = "ninja-nat-01"
  description = "tag for nat gateway"
}

variable vpc_id {
  type        = string
  
}
