variable "vpc-cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "aws assignment_04 vpc cidr vale"
}

variable "tenancy" {
  type        = string
  default     = "default"
  description = "instance tenancy"
}

variable "vpc-tag" {
  type        = string
  default     = "ninja-vpc-01"
  description = "vpc tag"
}

