resource "aws_route_table_association" "public_subnet_association" {
  count = length(var.pub-sub-cidr)
  subnet_id = var.pub-subnet-id[count.index]
  route_table_id = var.pub_RT_id
}

resource "aws_route_table_association" "private_subnet_association" {
  count          = length(var.pri_sub_cidr)
  subnet_id = var.pri_subnet_id[count.index]
  route_table_id = var.pri_RT_id
}

