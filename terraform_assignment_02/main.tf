#  resource "aws_s3_bucket_acl" "s3_bucket" {
    
#      bucket = "hrishab"
#      acl = "public-read-write"
#  }

# terraform {
#   backend "s3" {
#     encrypt = true    
#     bucket = "hrishab"
#     dynamodb_table = "terraform-state-lock-dynamo"
#     key    = "terraform.tfstate"
#     region = "ap-south-1"
#   }
# }

# resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
#   name = "terraform-state-lock-dynamo"
#   hash_key = "LockID"
#   read_capacity = 20
#   write_capacity = 20
 
#   attribute {
#     name = "LockID"
#     type = "S"
#   }
# }


module "vpc" {
  #depends_on = [module.subnet]
  source   = "./modules/vpc"
  vpc-cidr = var.vpc-cidr
  tenancy  = var.tenancy
  vpc-tag  = var.vpc-tag

}

module "IGW" {
  
  source = "./modules/igw"
  vpc_id = module.vpc.vpc_id
}


module "subnet" {

  source       = "./modules/subnet"
  vpc_id       = module.vpc.vpc_id
  pub-sub-cidr = var.pub-sub-cidr
  pri_sub_cidr = var.pri_sub_cidr
  azs          = var.azs



}

module "nat" {
  source        = "./modules/nat"
  nat_subnet_id = module.subnet.nat_subnet_id
  nat-tag       = var.nat-tag
}

module "route" {

  source          = "./modules/route"
  vpc_id          = module.vpc.vpc_id
  cidr_for_pub_RT = var.cidr_for_pub_RT
  IGW_id          = module.IGW.IGW_id
  pub-rt-tag      = var.pub-rt-tag
  cidr_for_pri_RT = var.cidr_for_pri_RT
  nat-gw_id       = module.nat.nat-gw_id
  priv-rt-tag     = var.priv-rt-tag

}

module "route_associastion" {
  source        = "./modules/route_associastion"
  pub-subnet-id = module.subnet.pub-subnet-id
  pub_RT_id     = module.route.pub_RT_id
  pri_subnet_id = module.subnet.pri_subnet_id
  pri_RT_id     = module.route.pri_RT_id
}

module "nacl" {
  source = "./modules/nacl"
  #count = length(var.nacl_ports)  
  vpc_id = module.vpc.vpc_id
  pub-subnet-id = module.subnet.pub-subnet-id
  nacl_ports = var.nacl_ports
  pri_subnet_id = module.subnet.pri_subnet_id
  # nacl_pri_ports = var.nacl_pri_ports[count.index]
  
}

module security {
  source = "./modules/security"
  public_sg_name = var.public_sg_name
  pub_sg_discription = var.pub_sg_discription
  vpc_id = module.vpc.vpc_id
  pub_ports = var.pub_ports
  pub_protocol = var.pub_protocol
  pub_cidr_block = var.pub_cidr_block
  private_sg_name = var.private_sg_name
  pri_sg_discription = var.pri_sg_discription
  pri_ports = var.pri_ports
  pri_protocol = var.pri_protocol
  pri_cidr_block = var.pri_cidr_block
}


module private_key {
  # depends_on = [module.route_associastion]
  source = "./modules/private_key"
  algorithm = var.algorithm
  key_name = var.key_name
}


module instance {
  source = "./modules/instance"
  pub-subnet-id = module.subnet.pub-subnet-id
  ami = var.ami
  key_name = module.private_key.keyname
  instance_type = var.instance_type
  pub_security_id = module.security.pub_security_id
  public_instance_name = var.public_server 
  pri_subnet_id = module.subnet.pri_subnet_id
  pri_security_id = module.security.pri_security_id
}
