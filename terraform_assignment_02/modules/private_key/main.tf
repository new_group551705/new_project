
resource "tls_private_key" "rsa" {
  algorithm =  var.algorithm
  rsa_bits  = 4096
}

resource "local_file" "ssh_key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = var.key_name
  file_permission = 400
}

resource "aws_key_pair" "pem_key" {
  key_name   = var.key_name
  public_key = tls_private_key.rsa.public_key_openssh
}


