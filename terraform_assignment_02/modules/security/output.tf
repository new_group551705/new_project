output pri_security_id {
  value       = aws_security_group.private_sg.id
}

output pub_security_id {
  value       = aws_security_group.public_sg.id
}