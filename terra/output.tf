output "vpc-id" {
  value = aws_vpc.vpc-01.id
}

output "pub-subnet-id" {
  value = aws_subnet.pub-subnets.*.id
}

output "pri_subnet_id" {
  value = aws_subnet.pri-subnets.*.id
}

output "nat_gw_id" {
  value = aws_nat_gateway.nat-gw.id
}

output "igw_id" {
  value = aws_internet_gateway.IGW.id
}

output "pri_route" {
  value = aws_route_table.pri-RT.id
}

output "pub_route" {
  value = aws_route_table.pub-RT.id
}

output "elastic_ip" {
  value = aws_eip.elastic-ip.id
}

output bastion_host_id {
  value       = aws_instance.bastion_host_server.*.id
}

output private_server_id {
  value       = aws_instance.private_server.*.id 
}

