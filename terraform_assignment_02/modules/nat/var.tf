variable nat-tag {
  type        = string
  default     = "ninja-01"
  description = "tags for nat-gateway"
}


variable nat_subnet_id  {
  type        = string
}
 
