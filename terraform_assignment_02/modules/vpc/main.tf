
resource "aws_vpc" "vpc-01" {
  cidr_block       = var.vpc-cidr
  instance_tenancy = var.tenancy
  tags = {
    Name = var.vpc-tag
  }
}

