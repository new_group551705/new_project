
variable vpc_id {
  type        = string
}

variable "cidr_for_pub_RT" {
  type        = string
  default     = "0.0.0.0/0"
  description = "to attach igw to internet"
}




variable "IGW_id" {
  type        = string
}



variable "pub-rt-tag" {
  type        = string
  default     = "public_RT"
  description = "tags for public route table"
}

variable "cidr_for_pri_RT" {
  type        = string
  default     = "0.0.0.0/0"
  description = "to attach nat-gw to internet"
}

variable "nat-gw_id" {
  type        = string
}

variable "priv-rt-tag" {
  type        = string
  default     = "private_RT"
  description = "tags for private route table"
}





