
resource "aws_route_table" "pub-RT" {
  vpc_id = var.vpc_id
  route {
  cidr_block = var.cidr_for_pub_RT
  gateway_id = var.IGW_id 
  }
  tags = {
    Name = var.pub-rt-tag
  }
}

resource "aws_route_table" "pri-RT" {
    vpc_id = var.vpc_id
  route {
    cidr_block     = var.cidr_for_pri_RT
    nat_gateway_id = var.nat-gw_id
  }
  tags = {
    Name = var.priv-rt-tag
  }
}


