
variable "ami" {
  type        = string
  default     = "ami-013cfb4ff88d86446"
  description = "ami for terraform assignment"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "what type of instance you want create"
}



variable "public_instance_name" {
  type        = string
  default     = "public_bastion_server"
  description = "provide name for your public server"
}

variable "pub-subnet-id" {
  type        = list(string)
}

variable "pri_subnet_id" {
  type        = list(string)
}


variable "key_name" {
  type        = string
  default = "pem_key"
}

variable "pub_security_id" {
  type        = string
}

variable "pri_security_id" {
  type        = string
}

variable public_server {
  type        = string
  default     = "public-01"
  description = "provide name for public instance"
}
